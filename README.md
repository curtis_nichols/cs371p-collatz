# CS371p: Object-Oriented Programming Collatz Repo

* Name: Curtis Nichols

* EID: cjn857

* GitLab ID: curtis_nichols

* HackerRank ID: nichols_curtis_j

* Git SHA: 24cee40751becc71dbd856755b7cbe5fcbc77ac1

* GitLab Pipelines: https://gitlab.com/curtis_nichols/cs371p-collatz/pipelines

* Estimated completion time: 10

* Actual completion time: 12

* Comments: Please comment on submission if everything was not submitted properly, so I can fix it next time. Note: Collatz cycle length code adapted from that shown in class.
