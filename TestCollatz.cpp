// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

long long cache[CACHE_SIZE];
// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    long long i;
    long long j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

//By Curtis: Test that reverse reads occur correctly
TEST(CollatzFixture, read_two) {
    istringstream r("1234 107\n");
    long long i;
    long long j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1234);
    ASSERT_EQ(j, 107);
}

//By Curtis: Test that we can read big and small enough numbers
TEST(CollatzFixture, read_three) {
    istringstream r("1 1000000\n");
    long long i;
    long long j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 1000000);
}
// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10, cache);
    ASSERT_EQ(v, 20);
}  //corrected from 11

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200, cache);
    ASSERT_EQ(v, 125);
} //corrected from 300

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210, cache);
    ASSERT_EQ(v, 89);
} //corrected from 411

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000, cache);
    ASSERT_EQ(v, 174);
} //corrected from 1900

//By Curtis: guarantee tests work in reverse order
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(1000, 900, cache);
    ASSERT_EQ(v, 174);
} //corrected from 1900

//By Curtis: test max and min values
TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(1, 1000000, cache);
    ASSERT_EQ(v, 525);
}

//By Curtis: test if values can be equal
TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(1000, 1000, cache);
    ASSERT_EQ(v, 112);
}

//By Curtis: test values above cache size
TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(1000010, 1000100, cache);
    ASSERT_EQ(v, 259);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

//By Curtis: Test longest and shortest possible prints
TEST(CollatzFixture, print_2) {
    ostringstream w;
    collatz_print(w, 1, 1000000, 525);
    ASSERT_EQ(w.str(), "1 1000000 525\n");
}


//By Curtis: Test reversed numbers
TEST(CollatzFixture, print_3) {
    ostringstream w;
    collatz_print(w, 1000000, 1, 525);
    ASSERT_EQ(w.str(), "1000000 1 525\n");
}


// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}

//By Curtis: Putting it altogether, reversed
TEST(CollatzFixture, solve_2) {
    istringstream r("10 1\n200 100\n210 201\n1000 900\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("10 1 20\n200 100 125\n210 201 89\n1000 900 174\n", w.str());
}

//By Curtis:
TEST(CollatzFixture, solve_3) {
    istringstream r("1 100000\n1 1000000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 100000 351\n1 1000000 525\n", w.str());
}
