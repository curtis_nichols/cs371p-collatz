// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

const long long CACHE_SIZE = 1000001;

using namespace std;


// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, long long& i, long long& j) {
    return r >> i >> j;
} //easily reads from r long longo i & j

// ------------
// cycle_length
// ------------

// cycle_length() adapted from Professor Downing's lecture notes at:
// https://gitlab.com/gpdowning/cs371p/blob/master/cpp/Assertions.cpp

long long cycle_length (long long n, long long *cache) {
    assert(n > 0); //won't assert for upper bound, because we could 3n+1 past it
    long long original = n;
    long long c = 1;
    while (n > 1) {
        if(n < CACHE_SIZE && cache[n] > 0) { //check cache whenever possible
            assert(n < CACHE_SIZE);
            c = c + (cache[n] - 1);
            n = 1;
        }
        else if ((n % 2) == 0) {
            n = (n / 2);
            ++c;
        }
        else { //Optimization from class for odd numbers
            n = n + (n/2) + 1;
            ++++c;
        }
    }
    assert(c > 0);
    if(original < CACHE_SIZE) {
        assert(original < CACHE_SIZE);
        cache[original] = c;
    }
    return c;
}

// ------------
// collatz_eval
// ------------

long long collatz_eval (long long i, long long j, long long *cache) {
    // <your code>
    assert(i > 0 && j > 0);
    long long max = 0;
    long long curr = 0;

    if(i > j)
    {   long long temp = j;
        j = i;
        i = temp;
    }

    for(long long n = i; n <= j; ++n) {
        curr = cycle_length(n, cache);
        if(curr > max)
            max = curr;
    }
    return max;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, long long i, long long j, long long v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    long long i;
    long long j;
    long long c[CACHE_SIZE];
    for(long long x = 0; x < CACHE_SIZE; x++) {
        c[x] = 0;
    } //zeroing out cache
    c[1] = 1;
    while (collatz_read(r, i, j)) {
        const long long v = collatz_eval(i, j, c);
        collatz_print (w, i, j, v);
    }
}